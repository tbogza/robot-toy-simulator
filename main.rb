require_relative 'robot'

puts "Welcome to the toy robot simulator!"
puts "Enter: PLACE X,Y,FACING"

robot = Robot.new

while input = gets.chomp
  break if input == 'exit'
  begin
    robot.parse(input)
  end
end