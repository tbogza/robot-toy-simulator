require 'spec_helper'
require_relative '../robot'

describe Robot do

  before :each do
    @robot = Robot.new
  end

  describe '#parse' do

    context 'VALID command' do
      it 'place move report' do
        @robot.parse('PLACE 0,0,NORTH')
        @robot.parse('MOVE')
        @robot.parse('REPORT')
        expect(@robot.x_position).to eq (0)
        expect(@robot.y_position).to eq (1)
        expect(@robot.direction).to eq ('NORTH')
      end

      it 'place left report' do
        @robot.parse('PLACE 0,0,NORTH')
        @robot.parse('LEFT')
        @robot.parse('REPORT')
        expect(@robot.x_position).to eq (0)
        expect(@robot.y_position).to eq (0)
        expect(@robot.direction).to eq ('WEST')
      end

      it 'place move move left move report' do
        @robot.parse('PLACE 1,2,EAST')
        @robot.parse('MOVE')
        @robot.parse('MOVE')
        @robot.parse('LEFT')
        @robot.parse('MOVE')
        @robot.parse('REPORT')
        expect(@robot.x_position).to eq (3)
        expect(@robot.y_position).to eq (3)
        expect(@robot.direction).to eq ('NORTH')
      end

      it 'place return true placed' do
        @robot.parse('PLACE 0,0,NORTH')
        expect(@robot.placed?).to eq (true)
      end
    end

    context 'INVALID command' do
      it 'returns nil' do
        input = 'fgshadfa'
        expect(@robot.parse(input)).to be nil
      end

      it 'move return false placed' do
        @robot.parse('MOVE')
        expect(@robot.placed?).to eq (false)
      end
    end
  end
end