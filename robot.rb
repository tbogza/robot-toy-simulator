class Robot
  PLACE_SIZE = 5
  attr_reader :x_position, :y_position, :direction

  def initialize
    @x_position = nil
    @y_position = nil
    @direction = nil
    @compass = ['SOUTH', 'WEST', 'NORTH', 'EAST']
  end

  def parse(input)
    command = input.gsub(',', ' ').split
    if command.include?('PLACE')
      parse_place(command)
      return
    else
      return unless placed?
      case
        when command.include?('MOVE')
          parse_move
          return
        when command.include?('LEFT') || command.include?('RIGHT')
          parse_turn(command, @direction)
          return
        when command.include?('REPORT')
          parse_report
          return
      end
    end
  end

  def parse_place(command)
    set_position(command[1].to_i, command[2].to_i)
    @direction = command[3]
  end

  def parse_move
    case @direction
      when 'NORTH'
        set_position(@x_position, @y_position + 1)
      when 'SOUTH'
        set_position(@x_position, @y_position - 1)
      when 'EAST'
        set_position(@x_position + 1, @y_position)
      when 'WEST'
        set_position(@x_position - 1, @y_position)
    end
  end

  def parse_turn(command, direction)
    index = @compass.index(direction)
    if command.include?('LEFT')
      @direction = @compass[index - 1]
    else
      @direction = @compass[index + 1]
    end
  end

  def parse_report
    puts "OUTPUT : #{@x_position.to_s},#{@y_position.to_s},#{@direction}"
  end

  def set_position(x, y)
    if x > PLACE_SIZE or y > PLACE_SIZE or x < 0 or y < 0
      puts 'Move is out of range'
    end
    @x_position = x
    @y_position = y
  end

  def placed?
    !@x_position.nil? && !@y_position.nil?
  end
end